
/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */
// The Office initialize function must be run each time a new page is loaded.
Office.initialize = function (reason) {


    var token = localStorage.getItem('token');
    var company_id;
    var project_id;
    var sprint_id;
    const apiUrl = "https://api.agileboard.me";
    var description;
    var mailUrl;

    var item = Office.context.mailbox.item;
    if (localStorage.getItem('token')) {
        item.body.getAsync('html', function (result) {
            if (result.status === 'succeeded') {
                description = result.value;
            }
        })
        document.getElementById("name").value = item.subject;
        token = localStorage.getItem('token');
        Company()
    }

    

    Office.onReady(info => {
        if (info.host === Office.HostType.Outlook) {
            document.getElementById("sideload-msg").style.display = "none";
            document.getElementById("app-body").style.display = "flex";
            document.getElementById("Company").onchange = Project;
            document.getElementById("Project").onchange = Sprint;
            document.getElementById("form-saving").onsubmit = ticket;
            document.getElementById("form-login").onsubmit = login;
            document.getElementById("logout").onclick = logout;

        }
    });

    
    function login() {
        $("#form-saving").show();

        item.body.getAsync('html', function (result) {
            if (result.status === 'succeeded') {
                description = result.value;
            }
        })

        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;

//Zalogowanie si� i pobranie tokena

        $.ajax({
            url: apiUrl + '/auth',
            dataType: 'json',
            method: "POST",
            data: {
                email: email,
                password: password
            }
        })
            .done(function (res) {
                localStorage.setItem('token', res.data.token);
                token = localStorage.getItem('token');
                $(".Button:hover").css("cursor", "wait");
                $(".Button:focus").css("cursor", "wait");
                $("body").css("cursor", "wait");

                Company()
            })
            .fail(() => {
                $("#loginError").show();
            });
        return false;
    }

//Wyb�r firmy
    function Company(){
        $.ajax({
            url: apiUrl + '/users/current/companies',
            dataType: 'json',
            headers: {
                Authorization: "Bearer " + token
            }
        })
            .done(function (res) {
                $('#Company')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="' + res.data[0]["id"] + '" selected>' + res.data[0]["name"] + '</option>');
                for (let i = 1; i < res.data.length; i++) {
                    $('#Company')
                        .append('<option value="' + res.data[i]["id"] + '" selected>' + res.data[i]["name"] + '</option>');
                }

                $('#Company').find('option:selected').removeAttr('selected');
                company_id = localStorage.getItem('company_id');
                $("#Company option[value='" + company_id + "']").attr("selected", "selected");
                
                Project();

            })
            .fail(() => {
                console.log("Company Error");
            });
        return false;
    }


//Wyb�r projektu
    function Project() {
            company_id = document.getElementById("Company").value;

            $.ajax({
                url: apiUrl + '/projects?has_access=1&selected_company_id=' + company_id,
                dataType: 'json',
                headers: {
                    Authorization: "Bearer " + token
                }
            })
                .done(function (res) {
                    $('#Project')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="' + res.data[0]["id"] + '" selected>' + res.data[0]["name"] + '</option>');
                    for (let i = 1; i < res.data.length; i++) {
                        $('#Project')
                            .append('<option value="' + res.data[i]["id"] + '" selected>' + res.data[i]["name"] + '</option>');
                    }

                    $('#Project option').removeAttr('selected');
                    project_id = localStorage.getItem('project_id');
                    $("#Project option[value='" + project_id + "']").attr("selected", "selected");

                    Sprint()

                })
                .fail(() => {
                    console.log("Project error");
                });
        return false;
        }

//Wyb�r sprintu
        function Sprint() {
            project_id = document.getElementById("Project").value;
            $.ajax({
                url: apiUrl + '/projects/' + project_id + '/sprints?status=not-closed&selected_company_id=' + company_id,
                dataType: 'json',
                headers: {
                    Authorization: "Bearer " + token
                },
            })
                .done(function (res) {
                    $('#Sprint')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="' + res.data[0]["id"] + '" selected>' + res.data[0]["name"] + '</option>');
                    for (let i = 1; i < res.data.length; i++) {
                        $('#Sprint')
                            .append('<option value="' + res.data[i]["id"] + '">' + res.data[i]["name"] + '</option>')
                    }
                    $('#Sprint')
                        .find('option')
                        .removeAttr('selected');
                    sprint_id = localStorage.getItem('sprint_id');
                    $("#Sprint option[value='" + sprint_id + "']").attr("selected", "selected");
                    if (document.getElementById('form-login').style !== "display: none;") {
                        show();
                    }
                })
                .fail(() => {
                    console.log("Sprint error");
                });
            return false;
        }
//Ukrycie ekranu logowania
        function show() {
            $(".formLogin").hide();
            $("body").css("cursor", "default");
            $(".Button:hover").css("cursor", "pointer");
            $(".Button:focus").css("cursor", "pointer");
            return false;
        }
//Stworzenie zadania
    function ticket() {





        localStorage.setItem('company_id', document.getElementById("Company").value);
        company_id = localStorage.getItem('company_id');
        localStorage.setItem('project_id', document.getElementById("Project").value);
        project_id = localStorage.getItem('project_id');
        localStorage.setItem('sprint_id', document.getElementById("Sprint").value);
        sprint_id = localStorage.getItem('sprint_id');

            var name = document.getElementById("name").value;

            $.ajax({
                url: apiUrl + '/projects/' + project_id + '/tickets',
                dataType: 'json',
                method: "POST",
                headers: {
                    Authorization: "Bearer " + token
                },
                data: {
                    selected_company_id: company_id,

                    name: name,
                    description: mailUrl+ "<hr/>"+description,
                    type_id: 2,
                    estimate_time: 0,
                    sprint_id: sprint_id
                }
            })
                .done(() => {
                    $(".success").show();
                    $("#form-saving").hide();
                })

                .fail(() => {
                    console.log("Ticket error ");
                });
        return false;
        }
//W��czenie ekranu logowania
    function logout() {
        $(".success").hide();

        $(".formLogin").show();
        localStorage.removeItem('token');
        localStorage.removeItem('company_id');
        localStorage.removeItem('project_id');
        localStorage.removeItem('sprint_id');
        };
    }

